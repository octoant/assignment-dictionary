global string_length
global print_string
global print_out
global print_err
global print_newline
global print_char
global print_int
global print_uint
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_equals
global string_copy
global trim_head
global trim_string
global trim_tail
global exit

%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60
%define STDIN 0
%define STDOUT 1
%define STDERR 2

section .text

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .iter:
    cmp byte[rdi+rax], 0
    je .break
    inc rax
    jmp .iter
  .break:
    ret

; Принимает указатель на нуль-терминированную строку и дескриптор файла, выводит её в файл
print_string:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_out:
    mov rsi, STDOUT
    jmp print_string

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err:
    mov rsi, STDERR
    jmp print_string

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, rsp
    sub rsp, 0x16
    dec rcx
    mov [rcx], byte 0
    mov r10, 10
  .iter:
    xor rdx, rdx
    div r10
    add rdx, '0'
    dec rcx
    mov [rcx], dl
    test rax, rax
    jne .iter
    mov rdi, rcx
    call print_string
    add rsp, 0x16
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, SYS_READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx. При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rsi-1              ; помещаем значения аргументов (-1 для нуль-терминатора) в стек,
    push rdi                ; чтобы не потерять их после вызова read_char
    xor r8, r8
    xor r9, r9
  .iter:
    call read_char          ; прочитаем очередной символ из стандартного потока ввода
    mov rdi, [rsp]          ; восстанавливаем значения аргументов используя стек,
    mov rsi, [rsp+1]        ; так как они уже там находятся
    cmp rax, 0x9
    je .check
    cmp rax, 0xa
    je .check
    cmp rax, 0x20
    je .check
    mov r9, 1               ; устанавливаем флаг того, что было прочитано не пробельный символ
    test rax, rax           ; сравниваем очередной символ с нуль-терминатором
    je .check               ; если они равны, то выходим из цикла (успешно из подпрограммы)
    cmp r8, rsi             ; сравниваем значение регистра <r8> с размером буфера
    je .err                 ; если они равны, то выходим из цикла (неуспешно из подпрограммы,
                            ; так как прочитанное слово не вмещается в буфер)
    mov [rdi+r8], al        ; иначе прописываем очередной символ в буфер
    inc r8                  ; увеличиваем размер на единицу, так как очередной символ прочитано
                            ; успешно
    jmp .iter
  .check:
    test r9, r9             ; проверяем того, что было ли прочитано не пробельный символ
    je .iter                ; если нет, то продолжаем цикл прочтения, иначе
    mov [rdi+r8], byte 0    ; дописываем к слову нуль-терминатор
    mov rax, rdi            ; записываем в аккумулятор адрес начала буфера
    mov rdx, r8             ; записываем в регистр данных длина прочитанной слова
    jmp .end
  .err:
    xor rax, rax
  .end:
    pop rdi                 ; восстанавливаем состояние стека,
    pop rsi                 ; которое было в начале подпрограммы
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер одна линия из стандартного потока ввода (stdin)
; Останавливается и возвращает -1 если слово слишком большое для буфера
; При успехе возвращает длину слова в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_line:
    xor r8, r8
  .iter:
    cmp rsi, r8
    je .err
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp rax, 0
    je .break
    cmp rax, 10
    je .break
    mov [rdi+r8], al
    inc r8
    jmp .iter
  .err:
    mov r8, -1
  .break:
    mov byte[rdi+r8], 0
    mov rax, r8
    ret

; Принимает указатель на строку, пытается прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 10
  .iter:
    mov r9b, [rdi+r8]
    cmp r9b, '0'
    jb .break
    cmp r9b, '9'
    ja .break
    sub r9b, '0'
    mul r10
    add rax, r9
    inc r8
    jmp .iter
  .break:
    mov rdx, r8
    ret

; Принимает указатель на строку, пытается прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r8, r8
    mov rax, 1
  .iter:
    mov r8b, [rdi+rcx]
    cmp r8b, [rsi+rcx]
    jne .err
    test r8b, r8b
    je .end
    inc rcx
    jmp .iter
  .err:
    xor rax, rax
  .end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
  .iter:
    cmp rax, rdx
    je .err
    mov r8b, [rdi+rax]
    mov [rsi+rax], r8b
    test r8b, r8b
    je .break
    inc rax
    jmp .iter
  .err:
    xor rax, rax
  .break:
    ret

; Принимает указатель на нуль-терминированную строку
; Удаляет начальные пробелы
trim_head:
    xor rax, rax
    mov rsi, rdi
    dec rsi
  .skip:
    inc rsi
    cmp byte[rsi], ' '
    je .skip
    cmp rsi, rdi
    je .end
  .iter:
    mov r8b, [rsi+rax]
    mov [rdi+rax], r8b
    test r8b, r8b
    je .end
    inc rax
    jmp .iter
  .end:
    ret

; Принимает указатель на нуль-терминированную строку
; Удаляет начальные и конечные пробелы
trim_string:
    push rdi
    call trim_head
    pop rdi

; Принимает указатель на нуль-терминированную строку
; Удаляет конечные пробелы
trim_tail:
    push rdi
    call string_length
    pop rdi
  .iter:
    mov byte[rdi+rax], 0
    dec rax
    cmp byte[rdi+rax], ' '
    je .iter
    ret

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall
